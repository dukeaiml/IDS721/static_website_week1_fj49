# Faraz Jawed's Portfolio

Welcome to my personal portfolio website. Here, I showcase my projects and share a bit about myself.

Check out the website [here](https://static-website-week1-fj49-fj49-d21349748027f6102aaf961ffe76bdc0.gitlab.io/)

## Introduction

This site is a static website, constructed using modern web technologies. It's designed to be a clean, simple platform where visitors can learn about my professional journey and explore the projects I've been a part of.

## Configuration

The website is configured through a `config.toml` file. Key configurations include:

- `base_url`: The root URL of the site.
- `theme`: The visual theme of the site.
- `title` and `description`: The site's title and a brief description.
- `output_dir`: The directory where the site is built.
- Various settings for content management, SEO optimization, and site features.

## Features

- **Project Showcase**: A dedicated section for showcasing my projects.
- **About Me**: A personal introduction and my professional background.
- **Responsive Design**: Ensures the site is accessible across various devices and screen sizes.

## Building the Site

To build and run the site locally:

1. Ensure you have the necessary tools installed (e.g., Zola).
2. Clone the repository.
3. Run `zola serve` in the project directory.
4. Access the local server at the provided URL.

## Contributing

Feel free to fork this project and use it as a template for your portfolio. If you have suggestions or improvements, I am open to pull requests.

## Contact

- **Email**: [faraz.jawed@duke.edu](mailto:faraz.jawed@duke.edu)
- **GitHub**: [github.com/farazjawedd](https://github.com/farazjawedd)
- **Twitter**: [twitter.com/farazjawedd](https://twitter.com/farazjawedd)

## License

This project is licensed under the [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/).
