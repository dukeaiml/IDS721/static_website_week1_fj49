+++
title = "NBA Stats visualization"
date = 2023-12-10
[extra]
# Any additional metadata you want to include
+++


## Project Overview

The NBA Statistics Dashboard is an innovative and user-focused project that harnesses daily data scraping to create a dynamic platform for NBA enthusiasts, fantasy league participants, and sports bettors. This cutting-edge tool offers a rich set of functionalities, including head-to-head analytics, raw statistics exploration, interactive visualization, team ranking insights, probability distribution graphs, point difference analytics, real-time injury updates, and dynamic raw stats graphs. By providing a comprehensive and customizable view of NBA team dynamics, the dashboard redefines the landscape of NBA statistics, empowering users to make informed decisions in fantasy leagues and sports betting.

To understand more about this project, kindly please see the source code on github or the youtube video demo (both linked below)

[Project_Github](https://github.com/nogibjj/BallersDash)

[Youtube_video](https://www.youtube.com/watch?v=vyATMDZy50Y&feature=youtu.be)





